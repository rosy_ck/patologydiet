function addMenu(Menu,currentDate,Patologia){
      var DietaConsigliata =getCookie("DietaConsigliata").replace("http://www.semanticweb.org/rosi/ontologies/PatologyDiet#","patologyDiet:");
if(DietaConsigliata){
  //creareMenucolazione
var InsertMenu=  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
+"PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"
+     "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
+"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
+"INSERT DATA{patologyDiet:colazione"+currentDate+" rdf:type patologyDiet:Colazione;"
+" patologyDiet:Giorno \""+currentDate+"\"^^xsd:string. patologyDiet:MenuColazione"+currentDate+" rdf:type patologyDiet:MenuColazione;"
+"patologyDiet:MenuColazione"+currentDate+" patologyDiet:MenuPerDieta "+ DietaConsigliata+"."
+"patologyDiet:Durante patologyDiet:colazione"+currentDate+"."
+"patologyDiet:Pranzo"+currentDate+" rdf:type patologyDiet:Pranzo;"
+" patologyDiet:Giorno \""+currentDate+"\"^^xsd:string. patologyDiet:MenuPranzo"+currentDate+" rdf:type patologyDiet:MenuPranzo;"
+"patologyDiet:Durante patologyDiet:Pranzo"+currentDate+
+"patologyDiet:MenuPranzo"+currentDate+" patologyDiet:MenuPerDieta "+ DietaConsigliata+"."
+"patologyDiet:Cena"+currentDate+" rdf:type patologyDiet:Cena;"
+" patologyDiet:Giorno \""+currentDate+"\"^^xsd:string. patologyDiet:MenuCena"+currentDate+" rdf:type patologyDiet:MenuCena;"
+"patologyDiet:MenuCena"+currentDate+" patologyDiet:MenuPerDieta "+ DietaConsigliata+"."
+"patologyDiet:Durante patologyDiet:Cena"+currentDate+"."
//Per ogni piatto Inserisci la voce
for(var key in Menu["colazione"]){
  //addVoceMenu
   InsertMenu +=" patologyDiet:Menu_V_"+Menu["colazione"][key]["nome"]+" rdf:type patologyDiet:Voce_Menu;"
 +" patologyDiet:TipoPiatto "+Menu["colazione"][key]["nome"]+";"
  " patologyDiet:Peso "+Menu["colazione"][key]["peso"]+"."
  +" patologyDiet:Menucolazione"+currentDate+" patologyDiet:FormatoDa  patologyDiet:Menu_V_"+Menu["colazione"][key]["nome"]+"."
}
for(var key in Menu["pranzo"]){
  //addVoceMenu
   InsertMenu +=" patologyDiet:Menu_V_"+Menu["pranzo"][key]["nome"]+" rdf:type patologyDiet:Voce_Menu;"
 +" patologyDiet:TipoPiatto "+Menu["pranzo"][key]["nome"]+";"
  " patologyDiet:Peso "+Menu["pranzo"][key]["peso"]+"."
  +" patologyDiet:MenuPranzo"+currentDate+" patologyDiet:FormatoDa  patologyDiet:Menu_V_"+Menu["pranzo"][key]["nome"]+"."
}
for(var key in Menu["cena"]){
  //addVoceMenu
   InsertMenu +=" patologyDiet:Menu_V_"+Menu["cena"][key]["nome"]+" rdf:type patologyDiet:Voce_Menu;"
 +" patologyDiet:TipoPiatto "+Menu["cena"][key]["nome"]+";"
  " patologyDiet:Peso "+Menu["cena"][key]["peso"]+"."
  +" patologyDiet:MenuCena"+currentDate+" patologyDiet:FormatoDa  patologyDiet:Menu_V_"+Menu["cena"][key]["nome"]+"."
}
InsertMenu += "}";
$.ajax({
    url: "http://localhost:7200/repositories/PatologyDiet_rosy",
             crossDomain: true,
             type:"POST",
             data:{'query':InsertMenu,'infer':false,'sameAs':true},
          //  dataType: 'json',
            success:function() {insert="OK"},
            error:function() {insert="Error"}
          });
          return insert;
}else{return "Error";}
}
