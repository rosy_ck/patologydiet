<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
  var UriAlimento=getUrlParameter("Alimento");
  var queryS="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
  "PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"+
"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
"SELECT  Distinct ?carboidrati ?proteine ?grassi ?saliMinerali ?vitamine"+
+"from <http://progetto/PatologyDiet>"+
"  WHERE {  "+UriAlimento+" patologyDiet:CarboidratiPer100g ?carboidrati;"+
" patologyDiet:LipidiPer100g ?grassi;"+
" patologyDiet:SaliMinerali ?saliMinerali;"+
" patologyDiet:Vitamine ?vitamine;"+
"patologyDiet:ProteinePer100g ?proteine.}";
  $.ajax({
    url: "http://localhost:7200/repositories/PatologyDiet_rosy",
             crossDomain: true,
             type:"POST",
             data:{'query':queryS,'infer':false,'sameAs':true},
            dataType: 'json',
           //header:{"Accept":"application/rdf+json"},
            success: function(data) {
               $.each(data["results"]["bindings"],function(i,item){
                $("h1").text(UriAlimento.replace("patologyDiet:",""));
                   $(".list-group").append("<li class=\"list-group-item\">PROTEINE: "+item["proteine"]["value"]+"</li>");
                  $(".list-group").append("<li class=\"list-group-item\">GRASSI: "+item["grassi"]["value"]+"</li>");
                  $(".list-group").append("<li class=\"list-group-item\">CARBOIDRATI: "+item["carboidrati"]["value"]+"</li>");
                        $(".list-group").append("<li class=\"list-group-item\">Sali Minerali: "+item["saliMinerali"]["value"]+"</li>");
                              $(".list-group").append("<li class=\"list-group-item\">Vitamine: "+item["vitamine"]["value"]+"</li>");
               });
             }
          });
});

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
</script>
</head>
<body>

<h1></h1>
<ul class="list-group">
</ul>

</body>
</html>
