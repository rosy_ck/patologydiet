<!DOCTYPE html>
<html>
<head>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="stile.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
  var queryS="PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"+
  +"from <http://progetto/PatologyDiet>"+
  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>SELECT ?patologia ?name WHERE { ?patologia rdf:type patologyDiet:Patologia;  patologyDiet:nome ?name} order by ?patologia";
$.ajax({
  url: "http://localhost:7200/repositories/PatologyDiet_rosy",
           crossDomain: true,
           type:"POST",
           data:{'query':queryS,'infer':true,'sameAs':true},
          dataType: 'json',
          success: function(data) {
            $.each(data["results"]["bindings"],function(i,item){
                $(".list-group").append("<li class=\"list-group-item\" id=\""+item["patologia"]["value"]+"\">"+item["name"]["value"]+"</li>")
            });
          }


});

$(document).on('click','li',function(){
var nomePatologia = $(this).text();
document.cookie="PatologiaScelta="+nomePatologia;
window.location.replace("CHOOSE.PHP")
});
});
</script>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Scegli Patologia</h5>

<p>Scegli la patologia per il quale si desidera controllare la dieta o aggiornarla.</p>
<ul class="list-group">
</ul>
            </div>
         </div>
      </div>
    </div>
  </div>

</body>
</html>
