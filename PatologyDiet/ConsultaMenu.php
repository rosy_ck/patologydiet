<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="stile.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="Funzioni.js"></script>
<script>
var currentDate=getUrlParameter("Data");
var Patologia=getCookie("PatologiaScelta");
//alert(Patologia);
$(document).ready(function(){
/*var d = new Date();
var month = d.getMonth()+1;
var day = d.getDate();
var currentDate = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day;
//var currentDate = "2020-01-12";
*/
var queryDate="PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"
+"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
+"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
"select  ?menu ?typeMenu ?NomePiatto  ?peso"+
+"from <http://progetto/PatologyDiet>"+
" where{?pasto rdf:type ?classePasto. ?classePasto rdfs:subClassOf  patologyDiet:Pasto; FILTER (?classePasto !=  patologyDiet:Pasto)."+
"?pasto patologyDiet:Giorno ?giorno. Filter contains(?giorno,\""+currentDate+"\")"
+".?menu patologyDiet:Durante ?pasto.FILTER (!isBlank(?typeMenu)).?menu rdf:type ?typeMenu. ?typeMenu rdfs:subClassOf* patologyDiet:Menu.?menu  patologyDiet:MenuPerDieta ?dieta."
+"patologyDiet:" + Patologia+"  patologyDiet:Consigliata ?dieta .FILTER (?typeMenu !=  patologyDiet:Menu)."+
"?voce patologyDiet:Peso ?peso; patologyDiet:TipoPiatto ?alimento. ?alimento patologyDiet:NomePiatto ?NomePiatto.?menu patologyDiet:FormatoDa ?voce}";
console.log(queryDate);
     $.ajax({
       url: "http://localhost:7200/repositories/PatologyDiet_rosy",
             crossDomain: true,
             type:"POST",
             data:{'query':queryDate,'infer':false,'sameAs':true},
             dataType: 'json',
            success: function(data) {
                if(data["results"]["bindings"].length>0){
                    $.each(data["results"]["bindings"],function(i,item){
                      var UrlMenu= item["typeMenu"]["value"];
                      if(UrlMenu.toLowerCase().indexOf("colazione")!=-1)
                        $("#colazione").find("table").find("tbody").append("<tr>").append("<td>"+item["NomePiatto"]["value"]+"</td><td>"+item["peso"]["value"]+"</td>");
                      else if  (UrlMenu.toLowerCase().indexOf("pranzo")!=-1)
                        $("#pranzo").find("table").find("tbody").append("<tr>").append("<td>"+item["NomePiatto"]["value"]+"</td><td>"+item["peso"]["value"]+"</td>");
                      else if  (UrlMenu.toLowerCase().indexOf("cena")!=-1)
                        $("#cena").find("table").find("tbody").append("<tr>").append("<td>"+item["NomePiatto"]["value"]+"</td><td>"+item["peso"]["value"]+"</td>");
                  });
                }else{
                   $("body div").hide();
                   $("body").append("<div class=\"alert alert-danger\" role=\"alert\">Non ci sono Menu disponibili per la data di oggi .<a onclick=\"goToMenu();\">Clicca qui per  creare il tuo menu </a></div>")
                   $(".alert.alert-danger").show();
               }
              }
            });
    });

    function goToMenu(){
 window.location.href = "CreatMenu.php?Data="+currentDate;
    }
</script>
</head>
<body>
  <div class="menuShow">
<div id="colazione" class="container row">
  <h1>Colazione</h1>
    <table class="table table-striped">
  <thead >
    <tr>
      <th scope="col">Nome Piatto</th>
      <th scope="col">Peso</th>
    </tr>
  </thead>
    <tbody></tbody>
      </table>
</div>
<div id="pranzo" class="container row"><h1>Pranzo</h1>
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Nome Piatto</th>
      <th scope="col">Peso</th>
    </tr>
  </thead>
    <tbody></tbody>
      </table>
</div>
<div id="cena" class="container row"><h1>Cena</h1>
    <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Nome Piatto</th>
      <th scope="col">Peso</th>
    </tr>
  </thead>
    <tbody></tbody>
    </table>
</div>
</div>
</body>
</html>
