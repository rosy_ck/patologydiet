<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="stile.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
  var queryS="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
  "PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"+
"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
"SELECT  ?alimentiConsigliati  ?nomeAlimento"+
"  WHERE { ?patologia rdf:type patologyDiet:Patologia;"+
" patologyDiet:nome ?nomePatologia; "+
" patologyDiet:Consigliata ?dieta."+
"?dieta    patologyDiet:AlimentiConsigliati  ?alimentiConsigliati;"+
"	FILTER regex(?nomePatologia, \"Diabete\")."+
"?alimentiConsigliati patologyDiet:NomeAlimento ?nomeAlimento. ?alimentiSConsigliati patologyDiet:NomeAlimento ?nomeAlimento."+
" } order by ?patologia";
  $.ajax({
    url: "http://localhost:7200/repositories/PatologyDiet_rosy",
             crossDomain: true,
             type:"POST",
             data:{'query':queryS,'infer':true,'sameAs':true},
            dataType: 'json',
           //header:{"Accept":"application/rdf+json"},
            success: function(data) {
              $.each(data["results"]["bindings"],function(i,item){
               $(".list-group.Consigliati").append("<li class=\"list-group-item clickMe\" id=\""+item["alimentiConsigliati"]["value"]+"\">"+item["nomeAlimento"]["value"]+"</li>");
              });
            }
          });
 queryS="PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
          "PREFIX patologyDiet: <http://www.semanticweb.org/rosi/ontologies/PatologyDiet#>"+
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
        "SELECT  ?alimentiSconsigliati  ?nomeAlimento"+
        +"from <http://progetto/PatologyDiet>"+
        "  WHERE { ?patologia rdf:type patologyDiet:Patologia;"+
        " patologyDiet:nome ?nomePatologia; "+
        " patologyDiet:Consigliata ?dieta."+
        "?dieta    patologyDiet:AlimentiDaEvitare  ?alimentiSconsigliati;"+
        "	FILTER regex(?nomePatologia, \"Diabete\")."+
        "?alimentiSconsigliati patologyDiet:NomeAlimento ?nomeAlimento. ?alimentiSconsigliati patologyDiet:NomeAlimento ?nomeAlimento."+
        " } order by ?patologia";
          $.ajax({
            url: "http://localhost:7200/repositories/PatologyDiet_rosy",
                     crossDomain: true,
                     type:"POST",
                     data:{'query':queryS,'infer':true,'sameAs':true},
                    dataType: 'json',
                   //header:{"Accept":"application/rdf+json"},
                    success: function(data) {
                      $.each(data["results"]["bindings"],function(i,item){
                       $(".list-group.Sconsigliati").append("<li class=\"list-group-item clickMe\" id=\""+item["alimentiSconsigliati"]["value"]+"\">"+item["nomeAlimento"]["value"]+"</li>");
                      });
                    }
                  });

});
$(document).on("click",".clickMe",function(){
  var  urlAlimento=$(this).attr("id").replace("http://www.semanticweb.org/rosi/ontologies/","").replace("PatologyDiet#","patologyDiet:");
  window.location="GetInfoAlimento.php?Alimento="+urlAlimento;
});
</script>
</head>
<body>
<div class="row CONSIGLIATI">
  <div class="col-sm-6">
<h1>ALIMENTI CONSIGLIATI</h1>
<ul class="list-group Consigliati">
</ul>
</div>
<div class="col-sm-6">
<h1> ALIMENTI SCONSIGLIATI</h1>
<ul class="list-group Sconsigliati">
</ul>
</div>
</div>
</body>
</html>
