\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Introduzione}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Motivazioni}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Requirements}{1}{subsection.1.2}
\contentsline {section}{\numberline {2}Documentazione}{2}{section.2}
\contentsline {section}{\numberline {3}Descrizione Ontologia}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Classi}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Object Properties}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}DataProperties}{11}{subsection.3.3}
\contentsline {section}{\numberline {4}Visuallizzazione}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}Tassonomia dell Classi}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Individui}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Triple}{13}{subsection.4.3}
\contentsline {section}{\numberline {5}FlowChart di iterazione}{15}{section.5}
\contentsline {section}{\numberline {6}Applicazione web}{19}{section.6}
\contentsline {subsection}{\numberline {6.1}Intro}{19}{subsection.6.1}
\contentsline {subparagraph}{App:}{19}{section*.2}
\contentsline {subparagraph}{Sparql Query:}{19}{section*.3}
\contentsline {subsection}{\numberline {6.2}Menu}{19}{subsection.6.2}
\contentsline {subparagraph}{App:}{19}{section*.4}
\contentsline {subparagraph}{Sparql Query:}{20}{section*.5}
\contentsline {subsection}{\numberline {6.3}Tabella Nutrizionale}{20}{subsection.6.3}
\contentsline {subparagraph}{App:}{20}{section*.6}
\contentsline {subparagraph}{Sparql Query:}{20}{section*.7}
\contentsline {subsection}{\numberline {6.4}Lista Alimenti Consigliati/Sconsigliati}{21}{subsection.6.4}
\contentsline {subparagraph}{App:}{21}{section*.8}
\contentsline {subparagraph}{Sparql Query:}{22}{section*.9}
\contentsline {subsection}{\numberline {6.5}Creazione Menu}{22}{subsection.6.5}
\contentsline {subparagraph}{App:}{22}{section*.10}
\contentsline {section}{\numberline {7}Bibliografia}{26}{section.7}
